package kraken

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

type Order struct {
	Id            string
	ReferealId    string
	UserReference string
	Status        string
	OpenTime      time.Time
	StartTime     time.Time
	ExpireTime    time.Time
	Info          struct {
		Pair             string
		Type             string
		OrderType        string
		PrimaryPrice     float64
		SecondaryPrice   float64
		Leverage         float64
		Description      string
		ConditionalClose string
	}
	Volume         float64
	VolumeExecuted float64
	Cost           float64
	Fee            float64
	Price          float64
	StopPrice      float64
	LimitPrice     float64
	Miscellaneus   string
	OrderFlags     string
	Trades         []string
}

func (o *Order) fromRaw(id string, r orderResponse) error {
	var err error
	o.Id = id
	o.ReferealId = r.RefId
	o.UserReference = r.UserReference
	o.Status = r.Status
	tu, tn := math.Modf(r.OpenTime)
	o.OpenTime = time.Unix(int64(tu), int64(tn*1e9)).UTC()
	tu, tn = math.Modf(r.StartTime)
	o.StartTime = time.Unix(int64(tu), int64(tn*1e9)).UTC()
	tu, tn = math.Modf(r.ExpireTime)
	o.ExpireTime = time.Unix(int64(tu), int64(tn*1e9)).UTC()
	o.Info.Pair = r.Info.Pair
	o.Info.Type = r.Info.Type
	o.Info.OrderType = r.Info.OrderType
	if o.Info.PrimaryPrice, err = strconv.ParseFloat(r.Info.PrimaryPrice, 64); err != nil {
		return err
	}
	if o.Info.SecondaryPrice, err = strconv.ParseFloat(r.Info.SecondaryPrice, 64); err != nil {
		return err
	}
	if o.Info.Leverage, err = strconv.ParseFloat(r.Info.Leverage, 64); err != nil {
		o.Info.Leverage = 0
	}
	o.Info.Description = r.Info.Description
	o.Info.ConditionalClose = r.Info.ConditionalClose
	if o.Volume, err = strconv.ParseFloat(r.Volume, 64); err != nil {
		return err
	}
	if o.VolumeExecuted, err = strconv.ParseFloat(r.VolumeExecuted, 64); err != nil {
		return err
	}
	if o.Cost, err = strconv.ParseFloat(r.Cost, 64); err != nil {
		return err
	}
	if o.Fee, err = strconv.ParseFloat(r.Fee, 64); err != nil {
		return err
	}
	if o.Price, err = strconv.ParseFloat(r.Price, 64); err != nil {
		return err
	}
	if o.StopPrice, err = strconv.ParseFloat(r.StopPrice, 64); err != nil {
		o.StopPrice = 0
	}
	if o.LimitPrice, err = strconv.ParseFloat(r.LimitPrice, 64); err != nil {
		o.LimitPrice = 0
	}
	o.Miscellaneus = r.Miscellaneus
	o.OrderFlags = r.OrderFlags
	o.Trades = strings.Split(r.Trades, ",")
	return nil
}

type OrderList []Order

type orderResponse struct {
	RefId         string  `json:"refid"`
	UserReference string  `json:"userref"`
	Status        string  `json:"status"`
	OpenTime      float64 `json:"opentm"`
	StartTime     float64 `json:"starttm"`
	ExpireTime    float64 `json:"expiretm"`
	Info          struct {
		Pair             string `json:"pair"`
		Type             string `json:"type"`
		OrderType        string `json:"ordertype"`
		PrimaryPrice     string `json:"price"`
		SecondaryPrice   string `json:"price2"`
		Leverage         string `json:"leverage"`
		Description      string `json:"order"`
		ConditionalClose string `json:"close"`
	} `json:"descr"`
	Volume         string `json:"vol"`
	VolumeExecuted string `json:"vol_exec"`
	Cost           string `json:"cost"`
	Fee            string `json:"fee"`
	Price          string `json:"price"`
	StopPrice      string `json:"stopprice"`
	LimitPrice     string `json:"limitprice"`
	Miscellaneus   string `json:"misc"`
	OrderFlags     string `json:"ofags"`
	Trades         string `json:"trades"`
}

func (k Kraken) OpenOrders() (OrderList, error) {
	response := &struct {
		Result map[string]map[string]orderResponse `json:"result"`
		Error  []string                            `json:"error"`
	}{}
	err := k.query("OpenOrders", nil, response)
	if err != nil {
		return nil, err
	}
	if len(response.Error) > 0 {
		return nil, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	ol := OrderList{}
	for oid, or := range response.Result["open"] {
		o := &Order{}
		err = o.fromRaw(oid, or)
		if err != nil {
			return nil, err
		}
		ol = append(ol, *o)
	}
	return ol, nil
}

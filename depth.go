package kraken

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"
)

type StandingOrder struct {
	Price  float64
	Volume float64
	Time   time.Time
}

func raw2DepthDataEntryList(data []interface{}) (OrderBoolList, error) {
	var err error
	de := make([]StandingOrder, len(data))
	for i, e := range data {
		d := e.([]interface{})
		de[i].Price, err = strconv.ParseFloat(d[0].(string), 64)
		if err != nil {
			return nil, err
		}
		de[i].Volume, err = strconv.ParseFloat(d[1].(string), 64)
		if err != nil {
			return nil, err
		}
		tu, tn := math.Modf(d[2].(float64))
		de[i].Time = time.Unix(int64(tu), int64(tn*1e9)).UTC()
	}
	return OrderBoolList(de), nil
}

type OrderBoolList []StandingOrder

func (sl OrderBoolList) toPrecission(precision int) OrderBoolList {
	round := func(num float64) int64 {
		return int64(num + math.Copysign(0.5, num))
	}
	ol := OrderBoolList{}
	pe := math.Pow(10, float64(precision))
	for _, s := range sl {
		precPrice := float64(round(s.Price*pe)) / pe
		found := false
		for i, o := range ol {
			if o.Price == precPrice {
				found = true
				ol[i].Volume += s.Volume
				if s.Time.Before(o.Time) {
					ol[i].Time = s.Time
				}
				break
			}
		}
		if !found {
			ol = append(ol, StandingOrder{precPrice, s.Volume, s.Time})
		}
	}
	return ol
}

func (sl OrderBoolList) scrapSpendingMoreThan(maxSpending float64) OrderBoolList {
	totalSpent := 0.0
	i := 0
	for ; i < len(sl); i++ {
		totalSpent += sl[i].Price * sl[i].Volume
		if totalSpent > maxSpending {
			break
		}
	}
	return append(OrderBoolList{}, sl[0:i]...)
}

func (a OrderBoolList) Len() int           { return len(a) }
func (a OrderBoolList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a OrderBoolList) Less(i, j int) bool { return a[i].Price < a[j].Price }

type OrderBook struct {
	Bids OrderBoolList
	Asks OrderBoolList
}

func (o OrderBook) AccumulateToPrecision(precision int) OrderBook {
	n := OrderBook{[]StandingOrder{}, []StandingOrder{}}
	n.Asks = o.Asks.toPrecission(precision)
	n.Bids = o.Bids.toPrecission(precision)
	sort.Sort(sort.Reverse(n.Bids))
	sort.Sort(n.Asks)
	return n
}

func (o OrderBook) SetMaxTotalSpending(maxSpending float64) OrderBook {
	sort.Sort(sort.Reverse(o.Bids))
	sort.Sort(o.Asks)
	n := OrderBook{[]StandingOrder{}, []StandingOrder{}}
	n.Bids = o.Bids.scrapSpendingMoreThan(maxSpending)
	n.Asks = o.Asks.scrapSpendingMoreThan(maxSpending)
	return n

}

func raw2OrderBook(data map[string]interface{}) (OrderBook, error) {
	var err error
	dr := OrderBook{}
	dr.Asks, err = raw2DepthDataEntryList(data["asks"].([]interface{}))
	if err != nil {
		return dr, err
	}
	dr.Bids, err = raw2DepthDataEntryList(data["bids"].([]interface{}))
	if err != nil {
		return dr, err
	}
	sort.Sort(dr.Asks)
	sort.Sort(dr.Bids)
	return dr, nil
}

func (k Kraken) Depth(pair string) (OrderBook, error) {
	response := &struct {
		Result map[string]interface{} `json:"result"`
		Error  []string               `json:"error"`
	}{}
	opts := map[string]string{"pair": pair, "count": "9999999"}
	ptr := OrderBook{}
	err := k.query("Depth", opts, response)
	if err != nil {
		return ptr, err
	}
	if len(response.Error) > 0 {
		return ptr, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	return raw2OrderBook(response.Result[pair].(map[string]interface{}))
}

package kraken

import (
	"fmt"
	"strconv"
)

type TradeBalanceResponse struct {
	EquivalentBalance float64
	TradeBalance      float64
	Margin            float64
	UnrealizedProfit  float64
	CostBasis         float64
	CurrentValuation  float64
	Equity            float64
	FreeMargin        float64
	MarginLevel       float64
}

func (k Kraken) TradeBalance(baseAsset string) (TradeBalanceResponse, error) {
	response := &struct {
		Result map[string]interface{} `json:"result"`
		Error  []string               `json:"error"`
	}{}
	tbr := TradeBalanceResponse{}
	opts := map[string]string{"asset": baseAsset}
	err := k.query("TradeBalance", opts, response)
	if err != nil {
		return tbr, err
	}
	if len(response.Error) > 0 {
		return tbr, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	if tbr.EquivalentBalance, err = strconv.ParseFloat(response.Result["eb"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.TradeBalance, err = strconv.ParseFloat(response.Result["tb"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.Margin, err = strconv.ParseFloat(response.Result["m"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.UnrealizedProfit, err = strconv.ParseFloat(response.Result["n"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.CostBasis, err = strconv.ParseFloat(response.Result["c"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.CurrentValuation, err = strconv.ParseFloat(response.Result["v"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.Equity, err = strconv.ParseFloat(response.Result["e"].(string), 64); err != nil {
		return tbr, err
	}
	if tbr.FreeMargin, err = strconv.ParseFloat(response.Result["mf"].(string), 64); err != nil {
		return tbr, err
	}
	if ml, ok := response.Result["ml"]; ok {
		if tbr.MarginLevel, err = strconv.ParseFloat(ml.(string), 64); err != nil {
			return tbr, err
		}
	}
	return tbr, nil
}

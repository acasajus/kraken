package kraken

import "fmt"

func (k Kraken) QueryOrders() (OrderList, error) {
	response := &struct {
		Result map[string]orderResponse `json:"result"`
		Error  []string                 `json:"error"`
	}{}
	err := k.query("QueryOrders", nil, response)
	if err != nil {
		return nil, err
	}
	if len(response.Error) > 0 {
		return nil, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	ol := OrderList{}
	for oid, or := range response.Result {
		o := &Order{}
		err = o.fromRaw(oid, or)
		if err != nil {
			return nil, err
		}
		ol = append(ol, *o)
	}
	return ol, nil
}

package kraken

import "fmt"

func (k Kraken) CancelOrder(txid string) error {
	response := &struct {
		Result struct {
			Count   int    `json:"count"`
			Pending string `json:"pending"`
		} `json:"result"`
		Error []string `json:"error"`
	}{}
	err := k.query("CancelOrder", map[string]string{"txid": txid}, response)
	if err != nil {
		return err
	}
	if len(response.Error) > 0 {
		return fmt.Errorf("Error in response: %s", response.Error[0])
	}
	return nil
}

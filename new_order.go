package kraken

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type NewOrderRequest struct {
	Pair          string
	Type          string
	OrderType     string
	Price         float64
	Price2        float64
	Leverage      float64
	Volume        float64
	Flags         []string
	StartTime     time.Time
	ExpiretTime   time.Time
	UserReference string
	ValidateOnly  bool
}

func (no NewOrderRequest) toMap() map[string]string {
	r := map[string]string{
		"pair":      no.Pair,
		"type":      no.Type,
		"ordertype": no.OrderType,
		"price":     strconv.FormatFloat(no.Price, 'f', -1, 64),
		"volume":    strconv.FormatFloat(no.Volume, 'f', -1, 64),
	}
	if no.Price2 > 0 {
		r["price2"] = strconv.FormatFloat(no.Price2, 'f', -1, 64)
	}
	if no.Leverage > 0 {
		r["leverage"] = strconv.FormatFloat(no.Leverage, 'f', -1, 64)
	}
	if no.Flags != nil {
		r["oflags"] = strings.Join(no.Flags, ",")
	}
	if !no.StartTime.IsZero() {
		r["starttm"] = strconv.FormatInt(no.StartTime.UTC().Unix(), 10)
	}
	if !no.ExpiretTime.IsZero() {
		r["expiretm"] = strconv.FormatInt(no.ExpiretTime.UTC().Unix(), 10)
	}
	if len(no.UserReference) > 0 {
		r["userref"] = no.UserReference
	}
	if no.ValidateOnly {
		r["validate"] = "true"
	}
	return r
}

type NewOrderResponse struct {
	Description struct {
		Order string `json:"order"`
		Close string `json:"close"`
	} `json:"descr"`
	TxIds string `json:"txid"`
}

func (k Kraken) NewOrder(no NewOrderRequest) (NewOrderResponse, error) {
	opts := no.toMap()
	response := &struct {
		Result NewOrderResponse `json:"result"`
		Error  []string         `json:"error"`
	}{}
	err := k.query("QueryOrders", opts, response)
	if err != nil {
		return response.Result, err
	}
	if len(response.Error) > 0 {
		return response.Result, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	return response.Result, nil
}

package kraken

import "strconv"

func (k Kraken) Balance() (map[string]float64, error) {
	response := &struct {
		Result map[string]string `json:"result"`
		Error  []string          `json:"error"`
	}{}
	err := k.query("Balance", nil, response)
	if err != nil {
		return nil, err
	}
	res := map[string]float64{}
	for k, v := range response.Result {
		res[k], err = strconv.ParseFloat(v, 64)
		if err != nil {
			return nil, err
		}
	}
	return res, nil
}

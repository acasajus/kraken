package kraken

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	// APIURL is the official Kraken API Endpoint
	APIURL = "https://api.kraken.com"
	// APIVersion is the official Kraken API Version Number
	APIVersion = "0"
	// APIUserAgent identifies this library with the Kraken API
	APIUserAgent = "SuperMegaUltraEvilBot"
)

var publicMethods = []string{
	"Time",
	"Assets",
	"AssetPairs",
	"Ticker",
	"Depth",
	"Trades",
	"Spread",
}

// List of valid private methods
var privateMethods = []string{
	"Balance",
	"TradeBalance",
	"OpenOrders",
	"ClosedOrders",
	"QueryOrders",
	"TradesHistory",
	"QueryTrades",
	"OpenPositions",
	"Ledgers",
	"QueryLedgers",
	"TradeVolume",
	"AddOrder",
	"CancelOrder",
}

type Kraken struct {
	key            string
	secret         string
	client         *http.Client
	debugResponses bool
}

func New(key, secret string) Kraken {
	client := &http.Client{}
	return Kraken{key, secret, client, false}
}

func NewDebug(key, secret string) Kraken {
	client := &http.Client{}
	return Kraken{key, secret, client, true}
}

// isStringInSlice is a helper function to test if given term is in a list of strings
func isStringInSlice(term string, list []string) bool {
	for _, found := range list {
		if term == found {
			return true
		}
	}
	return false
}

// getSha256 creates a sha256 hash for given []byte
func getSha256(input []byte) []byte {
	sha := sha256.New()
	sha.Write(input)
	return sha.Sum(nil)
}

// getHMacSha512 creates a hmac hash with sha512
func getHMacSha512(message, secret []byte) []byte {
	mac := hmac.New(sha512.New, secret)
	mac.Write(message)
	return mac.Sum(nil)
}

func createSignature(urlPath string, values url.Values, secret []byte) string {
	// See https://www.kraken.com/help/api#general-usage for more information
	shaSum := getSha256([]byte(values.Get("nonce") + values.Encode()))
	macSum := getHMacSha512(append([]byte(urlPath), shaSum...), secret)
	return base64.StdEncoding.EncodeToString(macSum)
}
func (k Kraken) query(method string, data map[string]string, out interface{}) error {
	values := url.Values{}
	for key, value := range data {
		values.Set(key, value)
	}
	if isStringInSlice(method, publicMethods) {
		return k.queryPublic(method, values, out)
	} else if isStringInSlice(method, privateMethods) {
		return k.queryPrivate(method, values, out)
	}

	return fmt.Errorf("Method '%s' is not valid!", method)
}

func (k Kraken) queryPublic(method string, values url.Values, out interface{}) error {
	url := fmt.Sprintf("%s/%s/public/%s", APIURL, APIVersion, method)
	return k.doRequest(url, values, nil, out)
}

func (k Kraken) queryPrivate(method string, values url.Values, out interface{}) error {
	urlPath := fmt.Sprintf("/%s/private/%s", APIVersion, method)
	reqURL := fmt.Sprintf("%s%s", APIURL, urlPath)
	secret, _ := base64.StdEncoding.DecodeString(k.secret)
	values.Set("nonce", fmt.Sprintf("%d", time.Now().UnixNano()))

	// Create signature
	signature := createSignature(urlPath, values, secret)

	// Add Key and signature to request headers
	headers := map[string]string{
		"API-Key":  k.key,
		"API-Sign": signature,
	}

	return k.doRequest(reqURL, values, headers, out)
}

func (k Kraken) doRequest(reqURL string, values url.Values, headers map[string]string, out interface{}) error {

	// Create request
	req, err := http.NewRequest("POST", reqURL, strings.NewReader(values.Encode()))
	if err != nil {
		return fmt.Errorf("Could not execute request! (%s)", err.Error())
	}

	req.Header.Add("User-Agent", APIUserAgent)
	for key, value := range headers {
		req.Header.Add(key, value)
	}

	// Execute request
	resp, err := k.client.Do(req)
	if err != nil {
		return fmt.Errorf("Could not execute request! (%s)", err.Error())
	}
	defer resp.Body.Close()

	if k.debugResponses {
		buf := bytes.NewBuffer(nil)
		buf.ReadFrom(resp.Body)
		log.Printf("Response was: %s", buf.String())
		err = json.NewDecoder(buf).Decode(out)
		if err != nil {
			return err
		}
		log.Println("Decoded is", out)
		return nil
	}

	return json.NewDecoder(resp.Body).Decode(out)
}

package kraken

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

type RecentTrade struct {
	Price     float64   `json:"price"`
	Volume    float64   `json:"volue"`
	Time      time.Time `json:"time"`
	Action    string    `json:"action"`
	OrderType string    `json:"order_type"`
	Misc      string    `json:"-"`
}

type RecentTradesResponse struct {
	Trades []RecentTrade
	Last   float64
}

func (k Kraken) RecentTrades(pair string, last float64) (RecentTradesResponse, error) {
	rtr := RecentTradesResponse{}
	response := &struct {
		Result map[string]interface{} `json:"result"`
		Error  []string               `json:"error"`
	}{}
	opts := map[string]string{"pair": pair}
	if last > 0 {
		opts["last"] = strconv.FormatFloat(last, 'f', -1, 64)
	}
	err := k.query("Trades", opts, response)
	if err != nil {
		return rtr, err
	}
	if len(response.Error) > 0 {
		return rtr, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	res := response.Result
	data := res[pair].([]interface{})
	if rtr.Last, err = strconv.ParseFloat(res["last"].(string), 64); err != nil {
		return rtr, err
	}
	rtr.Trades = make([]RecentTrade, len(data))
	for i, unk := range data {
		t := unk.([]interface{})
		if rtr.Trades[i].Price, err = strconv.ParseFloat(t[0].(string), 64); err != nil {
			return rtr, err
		}
		if rtr.Trades[i].Volume, err = strconv.ParseFloat(t[1].(string), 64); err != nil {
			return rtr, err
		}
		tu, tn := math.Modf(t[2].(float64))
		rtr.Trades[i].Time = time.Unix(int64(tu), int64(tn*1e9)).UTC()
		if t[3].(string) == "b" {
			rtr.Trades[i].Action = "buy"
		} else {
			rtr.Trades[i].Action = "sell"
		}
		if t[4].(string) == "m" {
			rtr.Trades[i].OrderType = "market"
		} else {
			rtr.Trades[i].OrderType = "limit"
		}
		rtr.Trades[i].Misc = t[5].(string)
	}
	return rtr, nil
}

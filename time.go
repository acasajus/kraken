package kraken

import "time"

const krakenTimeRFC1123Layoyt = "Mon, 02 Jan 06 15:04:05 -0700"

func (k Kraken) Time() (int64, time.Time, error) {
	st := struct {
		Error  []string `json:"error"`
		Result struct {
			Unixtime int64  `json:"unixtime"`
			RFCtime  string `json:"rfc1123"`
		} `json:"result"`
	}{}
	rfc := time.Time{}
	err := k.query("Time", map[string]string{}, &st)
	if err != nil {
		return st.Result.Unixtime, rfc, err
	}
	if rfc, err = time.Parse(krakenTimeRFC1123Layoyt, st.Result.RFCtime); err != nil {
		return st.Result.Unixtime, rfc, err
	}
	return st.Result.Unixtime, rfc, nil
}

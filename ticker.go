package kraken

import (
	"fmt"
	"strings"
)

type PairTickerFullVolume struct {
	Price       string
	Volume      string
	WholeVolume string
}

func raw2PairTickerFullVolume(d []interface{}) PairTickerFullVolume {
	return PairTickerFullVolume{d[0].(string), d[1].(string), d[2].(string)}
}

type PairTickerTime struct {
	Today   string
	Last24H string
}

func raw2PairTickerTime(d []interface{}) PairTickerTime {
	return PairTickerTime{d[0].(string), d[1].(string)}
}

type PairTicker struct {
	Ask       PairTickerFullVolume
	Bid       PairTickerFullVolume
	LastTrade struct {
		Price  string
		Volume string
	}
	Volume                PairTickerTime
	WeightedAverageVolume PairTickerTime
	TotalTrades           struct {
		Today   int64
		Last24H int64
	}
	LowPrice     PairTickerTime
	HighPrice    PairTickerTime
	OpeningPrice string
}

func raw2PairTicker(d map[string]interface{}) PairTicker {
	p := PairTicker{}
	p.Ask = raw2PairTickerFullVolume(d["a"].([]interface{}))
	p.Bid = raw2PairTickerFullVolume(d["b"].([]interface{}))
	lt := d["c"].([]interface{})
	p.LastTrade.Price = lt[0].(string)
	p.LastTrade.Volume = lt[1].(string)
	p.Volume = raw2PairTickerTime(d["v"].([]interface{}))
	p.WeightedAverageVolume = raw2PairTickerTime(d["p"].([]interface{}))
	tt := d["t"].([]interface{})
	p.TotalTrades.Today = int64(tt[0].(float64))
	p.TotalTrades.Last24H = int64(tt[1].(float64))
	p.LowPrice = raw2PairTickerTime(d["l"].([]interface{}))
	p.HighPrice = raw2PairTickerTime(d["h"].([]interface{}))
	p.OpeningPrice = d["o"].(string)
	return p
}

type PairTickerResponse map[string]PairTicker

func (k Kraken) Ticker(pairs ...string) (PairTickerResponse, error) {
	if len(pairs) == 0 {
		return nil, fmt.Errorf("For which pairs do you want the ticker?")
	}
	response := &struct {
		Result map[string]interface{} `json:"result"`
		Error  []string               `json:"error"`
	}{}
	opts := map[string]string{"pair": strings.Join(pairs, ",")}
	err := k.query("Ticker", opts, response)
	if err != nil {
		return nil, err
	}
	if len(response.Error) > 0 {
		return nil, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	ptr := PairTickerResponse{}
	for pairName, d := range response.Result {
		ptr[pairName] = raw2PairTicker(d.(map[string]interface{}))
	}
	return ptr, nil
}

package kraken

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

type SpreadEntry struct {
	Time time.Time
	Bid  float64
	Ask  float64
}

type SpreadResponse struct {
	Entries []SpreadEntry
	Last    float64
}

func raw2SpreadResponse(data []interface{}) (SpreadResponse, error) {
	var err error
	de := make([]SpreadEntry, len(data))
	for i, e := range data {
		d := e.([]interface{})
		tu, tn := math.Modf(d[0].(float64))
		de[i].Time = time.Unix(int64(tu), int64(tn*1e9)).UTC()
		de[i].Bid, err = strconv.ParseFloat(d[1].(string), 64)
		if err != nil {
			return SpreadResponse{}, err
		}
		de[i].Ask, err = strconv.ParseFloat(d[2].(string), 64)
		if err != nil {
			return SpreadResponse{}, err
		}
	}
	return SpreadResponse{de, 0}, nil
}

func (k Kraken) Spread(pair string, since float64) (SpreadResponse, error) {
	response := &struct {
		Result map[string]interface{} `json:"result"`
		Error  []string               `json:"error"`
	}{}
	opts := map[string]string{"pair": pair}
	if since != 0 {
		opts["since"] = strconv.FormatFloat(since, 'f', -1, 64)
	}
	ptr := SpreadResponse{}
	err := k.query("Spread", opts, response)
	if err != nil {
		return ptr, err
	}
	if len(response.Error) > 0 {
		return ptr, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	ptr, err = raw2SpreadResponse(response.Result[pair].([]interface{}))
	if err != nil {
		return ptr, err
	}
	ptr.Last = response.Result["last"].(float64)
	return ptr, nil
}

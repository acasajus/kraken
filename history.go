package kraken

import (
	"fmt"
	"math"
	"time"
)

type TradeMap map[string]Trade

func (tm TradeMap) GetStartAndEndTime() (start time.Time, end time.Time) {
	start = time.Now()
	for _, trade := range tm {
		i, d := math.Modf(trade.Time)
		t := time.Unix(int64(i), int64(d*1e9)).UTC()
		if t.Before(start) {
			start = t
		}
		if t.After(end) {
			end = t
		}
	}
	return
}

type HistoryResponse struct {
	Trades TradeMap `json:"trades"`
	Count  int      `json:"count"`
}

type Trade struct {
	OrderTxID                 string  `json:"ordertxid"`
	Pair                      string  `json:"pair"`
	Time                      float64 `json:"time"`
	OrderType                 string  `json:"ordertype"`
	Price                     string  `json:"price"`
	Cost                      string  `json:"cost"`
	Fee                       string  `json:"fee"`
	Volume                    string  `json:"vol"`
	Margin                    string  `json:"margin"`
	Misc                      string  `json:"misc"`
	Closing                   string  `json:"closing"`
	PostStatus                string  `json:"posstatus"`
	ClosedPortionAveragePrice string  `json:"cprice"`
	ClosedPortionTotalCost    string  `json:"ccost"`
	ClosedPortionFee          string  `json:"cfee"`
	ClosedPortionFreedMargin  string  `json:"cmargin"`
	NetProfit                 string  `json:"net"`
	PositionTrades            string  `json:"trades"`
}

func (k Kraken) History() (HistoryResponse, error) {
	response := &struct {
		Result HistoryResponse `json:"result"`
		Error  []string        `json:"error"`
	}{}
	err := k.query("TradesHistory", map[string]string{}, response)
	if err != nil {
		return response.Result, err
	}
	if len(response.Error) > 0 {
		return response.Result, fmt.Errorf("Error in response: %s", response.Error[0])
	}
	return response.Result, nil
}
